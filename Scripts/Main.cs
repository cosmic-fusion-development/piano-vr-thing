using System.Collections.Generic;

public class MainController
{
    static void Main(string[] args)
    {
        MelodyFactory mFactory = new MelodyFactory(new KeySig("C"), new TimeSig(4, 4));
        mFactory.GenerateMelody();
    }
}
