package com.cfc.piandroid;
import com.unity3d.player.UnityPlayerActivity;
import android.os.Bundle;
import android.util.Log;

import android.media.midi.*;
import android.media.midi.MidiDeviceInfo.PortInfo;

//import android.media.midi.MidiDeviceStatus;
//import android.media.midi.MidiReciever;
//import android.widget.PopupWindow;

import com.unity3d.player.UnityPlayer;

import java.io.IOException;



public class JMidiKeyboardManager extends UnityPlayerActivity
{
    private MidiManager mMidiManager;
    private MidiReceiver mLoggingReceiver;
    private MidiFramer mConnectFramer;
    private MyDirectReceiver mDirectReceiver;
    private String mLog;

    public String getLog()
    {
        return mLog;
    }

    public String getLogAndClear()
    {
        String returnLog = mLog;
        mLog = "";
        return returnLog;
    }

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Log.d("MidiKeyboardManager", "onCreate");

        // Create a Midi Manager
        mMidiManager = (MidiManager) getSystemService(MIDI_SERVICE);

        // Receiver that prints the messages.
        mLoggingReceiver = new LoggingReceiver();

        // Receiver that parses raw data into complete messages.
        mConnectFramer = new MidiFramer(mLoggingReceiver);

        mDirectReceiver = new MyDirectReceiver();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }



    /////////////////////////////////////////////////
    public class MidiScope extends MidiDeviceService
    {
        private static final String TAG = "MidiScope";

        private MidiReceiver mInputReceiver = new MyReceiver();

        @Override
        public void onCreate() {
            super.onCreate();
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
        }

        @Override
        public MidiReceiver[] onGetInputPortReceivers() {
            return new MidiReceiver[] { mInputReceiver };
        }

        class MyReceiver extends MidiReceiver
        {
            @Override
            public void onSend(byte[] data, int offset, int count,
                    long timestamp) throws IOException {
    //            if (mScopeLogger != null) {
                    // Send raw data to be parsed into discrete messages.
//                    mDeviceFramer.send(data, offset, count, timestamp);
    //            }
            }
        }

        /**
         * This will get called when clients connect or disconnect.
         * Log device information.
         */
        @Override
        public void onDeviceStatusChanged(MidiDeviceStatus status)
        {
    //        if (mScopeLogger != null) {
                if (status.isInputPortOpen(0))
                {
                    MyLog("=== connected ===");
                    String text = MidiPrinter.formatDeviceInfo(
                            status.getDeviceInfo());
                    MyLog(text);
                }
                else
                {
                    MyLog("--- disconnected ---");
                }
//            }
        }
    }

    /////////////////////////////////////////////////////////////////////


    /////////////// MYDIRECTRECEIVER /////////////////////////
    class MyDirectReceiver extends MidiReceiver
    {
        @Override
        public void onSend(byte[] data, int offset, int count,
                long timestamp) throws IOException
        {
            // Send raw data to be parsed into discrete messages.
            mConnectFramer.send(data, offset, count, timestamp);
        }
    }
    ////////////////////////////////////////////////////



    public void spamConsole()
    {
        Log.d("aaaaa", "Hello virtual world!");
        return;
    }


    public String testMethod()
    {
        Log.d("testMethod", "Hello virtual world!");
        return "Hello virtual world!";
    }



    /////////////   MIDI_FRAMER ///////////////////////////
    class MidiFramer extends MidiReceiver
    {
        private MidiReceiver mReceiver;
        private byte[] mBuffer = new byte[3];
        private int mCount;
        private byte mRunningStatus;
        private int mNeeded;
        private boolean mInSysEx;

        public MidiFramer(MidiReceiver receiver)
        {
            mReceiver = receiver;
        }

        /*
         * @see android.midi.MidiReceiver#onSend(byte[], int, int, long)
         */
        @Override
        public void onSend(byte[] data, int offset, int count, long timestamp)
                throws IOException
        {
            int sysExStartOffset = (mInSysEx ? offset : -1);

            for (int i = 0; i < count; i++)
            {
                final byte currentByte = data[offset];
                final int currentInt = currentByte & 0xFF;
                if (currentInt >= 0x80)
                { // status byte?
                    if (currentInt < 0xF0)
                    { // channel message?
                        mRunningStatus = currentByte;
                        mCount = 1;
                        mNeeded = MidiConstants.getBytesPerMessage(currentByte) - 1;
                    }
                    else if (currentInt < 0xF8)
                    { // system common?
                        if (currentInt == 0xF0 /* SysEx Start */)
                        {
                            // Log.i(TAG, "SysEx Start");
                            mInSysEx = true;
                            sysExStartOffset = offset;
                        }
                        else if (currentInt == 0xF7 /* SysEx End */)
                        {
                            // Log.i(TAG, "SysEx End");
                            if (mInSysEx)
                            {
                                mReceiver.send(data, sysExStartOffset,
                                    offset - sysExStartOffset + 1, timestamp);
                                mInSysEx = false;
                                sysExStartOffset = -1;
                            }
                        }
                        else
                        {
                            mBuffer[0] = currentByte;
                            mRunningStatus = 0;
                            mCount = 1;
                            mNeeded = MidiConstants.getBytesPerMessage(currentByte) - 1;
                        }
                    }
                    else
                    { // real-time?
                        // Single byte message interleaved with other data.
                        if (mInSysEx)
                        {
                            mReceiver.send(data, sysExStartOffset,
                                    offset - sysExStartOffset, timestamp);
                            sysExStartOffset = offset + 1;
                        }
                        mReceiver.send(data, offset, 1, timestamp);
                    }
                }
                else
                { // data byte
                    if (!mInSysEx)
                    {
                        mBuffer[mCount++] = currentByte;
                        if (--mNeeded == 0)
                        {
                            if (mRunningStatus != 0)
                            {
                                mBuffer[0] = mRunningStatus;
                            }
                            mReceiver.send(mBuffer, 0, mCount, timestamp);
                            mNeeded = MidiConstants.getBytesPerMessage(mBuffer[0]) - 1;
                            mCount = 1;
                        }
                    }
                }
                ++offset;
            }

            // send any accumulatedSysEx data
            if (sysExStartOffset >= 0 && sysExStartOffset < offset)
            {
                mReceiver.send(data, sysExStartOffset,
                        offset - sysExStartOffset, timestamp);
            }
        }
    }
    ///////////////////////////////////////////





    ////////////////////////// LOGGINGRECEIVER /////////////////////////

    /**
     * Convert incoming MIDI messages to a string and write them to a ScopeLogger.
     * Assume that messages have been aligned using a MidiFramer.
     */
    public class LoggingReceiver extends MidiReceiver
    {
        public static final String TAG = "MidiScope";
        private static final long NANOS_PER_MILLISECOND = 1000000L;
        private static final long NANOS_PER_SECOND = NANOS_PER_MILLISECOND * 1000L;
        private long mStartTime;
//        private ScopeLogger mLogger;
        private long mLastTimeStamp = 0;

        public LoggingReceiver()
        {
            mStartTime = System.nanoTime();
//            mLogger = logger;
        }

        /*
         * @see android.media.midi.MidiReceiver#onSend(byte[], int, int, long)
         */
        @Override
        public void onSend(byte[] data, int offset, int count, long timestamp)
                throws IOException
        {
            StringBuilder sb = new StringBuilder();
            if (timestamp == 0)
            {
                sb.append(String.format("-----0----: "));
            }
            else
            {
                long monoTime = timestamp - mStartTime;
                long delayTimeNanos = timestamp - System.nanoTime();
                int delayTimeMillis = (int)(delayTimeNanos / NANOS_PER_MILLISECOND);
                double seconds = (double) monoTime / NANOS_PER_SECOND;
                // Mark timestamps that are out of order.
                sb.append((timestamp < mLastTimeStamp) ? "*" : " ");
                mLastTimeStamp = timestamp;
                sb.append(String.format("%10.3f (%2d): ", seconds, delayTimeMillis));
            }
            sb.append(MidiPrinter.formatBytes(data, offset, count));
            sb.append(": ");
            sb.append(MidiPrinter.formatMessage(data, offset, count));
            String text = sb.toString();
            MyLog(text);
            Log.i(TAG, text);
        }

    }

    ///////////////////////////////////////////////

    public void MyLog(String text)
    {
        mLog = mLog + "\n" + text;
    }
}

//////////////////////////////////////////////////////
/**
 * Format a MIDI message for printing.
 */
class MidiPrinter {

    public static final String[] CHANNEL_COMMAND_NAMES = { "NoteOff", "NoteOn",
            "PolyTouch", "Control", "Program", "Pressure", "Bend" };
    public static final String[] SYSTEM_COMMAND_NAMES = { "SysEx", // F0
            "TimeCode",    // F1
            "SongPos",     // F2
            "SongSel",     // F3
            "F4",          // F4
            "F5",          // F5
            "TuneReq",     // F6
            "EndSysex",    // F7
            "TimingClock", // F8
            "F9",          // F9
            "Start",       // FA
            "Continue",    // FB
            "Stop",        // FC
            "FD",          // FD
            "ActiveSensing", // FE
            "Reset"        // FF
    };

    public static String getName(int status)
    {
        if (status >= 0xF0)
        {
            int index = status & 0x0F;
            return SYSTEM_COMMAND_NAMES[index];
        }
        else if (status >= 0x80)
        {
            int index = (status >> 4) & 0x07;
            return CHANNEL_COMMAND_NAMES[index];
        }
        else
        {
            return "data";
        }
    }

    public static String formatBytes(byte[] data, int offset, int count)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < count; i++)
        {
            sb.append(String.format(" %02X", data[offset + i]));
        }
        return sb.toString();
    }

    // This assumes the message has been aligned using a MidiFramer
    // so that the first byte is a status byte.
    public static String formatMessage(byte[] data, int offset, int count)
    {
        StringBuilder sb = new StringBuilder();
        byte statusByte = data[offset++];
        int status = statusByte & 0xFF;
        sb.append(getName(status)).append("(");
        int numData = MidiConstants.getBytesPerMessage(statusByte) - 1;
        if ((status >= 0x80) && (status < 0xF0))
        { // channel message
            int channel = status & 0x0F;
            // Add 1 for humans who think channels are numbered 1-16.
            sb.append((channel + 1)).append(", ");
        }
        for (int i = 0; i < numData; i++)
        {
            if (i > 0)
            {
                sb.append(", ");
            }
            sb.append(data[offset++]);
        }
        sb.append(")");
        return sb.toString();
    }

    public static String formatDeviceInfo(MidiDeviceInfo info)
    {
        StringBuilder sb = new StringBuilder();
        if (info != null)
        {
            Bundle properties = info.getProperties();
            for (String key : properties.keySet())
            {
                Object value = properties.get(key);
                sb.append(key).append(" = ").append(value).append('\n');
            }
            for (PortInfo port : info.getPorts())
            {
                sb.append((port.getType() == PortInfo.TYPE_INPUT) ? "input"
                        : "output");
                sb.append("[").append(port.getPortNumber()).append("] = \"").append(port.getName()
                        + "\"\n");
            }
        }
        return sb.toString();
    }
}
////////////////////////////////////



///////////////////////////////////////////////

/**
 * MIDI related constants and static methods.
 * These values are defined in the MIDI Standard 1.0
 * available from the MIDI Manufacturers Association.
 */
class MidiConstants
{
    protected final static String TAG = "MidiTools";
    public static final byte STATUS_COMMAND_MASK = (byte) 0xF0;
    public static final byte STATUS_CHANNEL_MASK = (byte) 0x0F;

    // Channel voice messages.
    public static final byte STATUS_NOTE_OFF = (byte) 0x80;
    public static final byte STATUS_NOTE_ON = (byte) 0x90;
    public static final byte STATUS_POLYPHONIC_AFTERTOUCH = (byte) 0xA0;
    public static final byte STATUS_CONTROL_CHANGE = (byte) 0xB0;
    public static final byte STATUS_PROGRAM_CHANGE = (byte) 0xC0;
    public static final byte STATUS_CHANNEL_PRESSURE = (byte) 0xD0;
    public static final byte STATUS_PITCH_BEND = (byte) 0xE0;

    // System Common Messages.
    public static final byte STATUS_SYSTEM_EXCLUSIVE = (byte) 0xF0;
    public static final byte STATUS_MIDI_TIME_CODE = (byte) 0xF1;
    public static final byte STATUS_SONG_POSITION = (byte) 0xF2;
    public static final byte STATUS_SONG_SELECT = (byte) 0xF3;
    public static final byte STATUS_TUNE_REQUEST = (byte) 0xF6;
    public static final byte STATUS_END_SYSEX = (byte) 0xF7;

    // System Real-Time Messages
    public static final byte STATUS_TIMING_CLOCK = (byte) 0xF8;
    public static final byte STATUS_START = (byte) 0xFA;
    public static final byte STATUS_CONTINUE = (byte) 0xFB;
    public static final byte STATUS_STOP = (byte) 0xFC;
    public static final byte STATUS_ACTIVE_SENSING = (byte) 0xFE;
    public static final byte STATUS_RESET = (byte) 0xFF;

    /** Number of bytes in a message nc from 8c to Ec */
    public final static int CHANNEL_BYTE_LENGTHS[] = { 3, 3, 3, 3, 2, 2, 3 };

    /** Number of bytes in a message Fn from F0 to FF */
    public final static int SYSTEM_BYTE_LENGTHS[] = { 1, 2, 3, 2, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1 };

    public final static int MAX_CHANNELS = 16;

    /**
     * MIDI messages, except for SysEx, are 1,2 or 3 bytes long.
     * You can tell how long a MIDI message is from the first status byte.
     * Do not call this for SysEx, which has variable length.
     * @param statusByte
     * @return number of bytes in a complete message, zero if data byte passed
     */
    public static int getBytesPerMessage(byte statusByte) {
        // Java bytes are signed so we need to mask off the high bits
        // to get a value between 0 and 255.
        int statusInt = statusByte & 0xFF;
        if (statusInt >= 0xF0) {
            // System messages use low nibble for size.
            return SYSTEM_BYTE_LENGTHS[statusInt & 0x0F];
        } else if(statusInt >= 0x80) {
            // Channel voice messages use high nibble for size.
            return CHANNEL_BYTE_LENGTHS[(statusInt >> 4) - 8];
        } else {
            return 0; // data byte
        }
    }

    /**
     * @param msg
     * @param offset
     * @param count
     * @return true if the entire message is ActiveSensing commands
     */
    public static boolean isAllActiveSensing(byte[] msg, int offset,
            int count) {
        // Count bytes that are not active sensing.
        int goodBytes = 0;
        for (int i = 0; i < count; i++) {
            byte b = msg[offset + i];
            if (b != MidiConstants.STATUS_ACTIVE_SENSING) {
                goodBytes++;
            }
        }
        return (goodBytes == 0);
    }

}
/////////////////////////////////////////////////
