public class AbstractChord
{
    public AbstractNote _root;
    public string _chord;

    private AbstractChord() {}

    public AbstractChord(string chordSymbol)
    {
        switch (chordSymbol)
        {
            case "I":
            _root = new AbstractNote(0);
            _chord = "major";
            break;

            case "i":
            _root = new AbstractNote(0);
            _chord = "minor";
            break;

            case "II":
            _root = new AbstractNote(2);
            _chord = "major";
            break;

            case "ii":
            _root = new AbstractNote(2);
            _chord = "minor";
            break;

            case "III":
            _root = new AbstractNote(4);
            _chord = "major";
            break;

            case "iii":
            _root = new AbstractNote(4);
            _chord = "minor";
            break;

            case "IV":
            _root = new AbstractNote(5);
            _chord = "major";
            break;

            case "iv":
            _root = new AbstractNote(5);
            _chord = "minor";
            break;

            case "V":
            _root = new AbstractNote(7);
            _chord = "major";
            break;

            case "v":
            _root = new AbstractNote(7);
            _chord = "minor";
            break;

            case "VI":
            _root = new AbstractNote(9);
            _chord = "major";
            break;

            case "vi":
            _root = new AbstractNote(9);
            _chord = "minor";
            break;

            case "VII":
            _root = new AbstractNote(11);
            _chord = "major";
            break;

            case "vii":
            _root = new AbstractNote(11);
            _chord = "minor";
            break;
        }
    }

    public override string ToString()
    {
        string rootstr = _root.GetScaleNumString();
        if (_chord == "minor")
        {
            rootstr = rootstr.ToLower();
        }

        return rootstr;
    }
}
