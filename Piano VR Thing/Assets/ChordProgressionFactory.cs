using System;
using System.Collections.Generic;

public class ChordProgressionFactory
{
    private Dictionary<string, List<string> > _PrevChords;

    public ChordProgressionFactory()
    {
        _PrevChords = new Dictionary<string, List<string> >();

        _PrevChords.Add("I", new List<string> () {"IV", "V"} );
        _PrevChords.Add("IV", new List<string> () {"iii", "vi", "I"} );
        _PrevChords.Add("V", new List<string> () {"ii", "IV", "I"} );
        _PrevChords.Add("ii", new List<string>() {"IV", "vi", "I"} );
        _PrevChords.Add("vi", new List<string> () {"V", "iii", "I"} );
        _PrevChords.Add("iii", new List<string>() {"ii", "V", "I"} );
    }

    public List<string> GenerateChordProgression(int numMeasures)
    {
        Random rand = new Random();
        List<string> chordProg = new List<string> ();
        chordProg.Add("I");
        for (int i=1; i<numMeasures-1; i++)
        {
            List<string> choices = _PrevChords[chordProg[chordProg.Count-1]];
            int idx = rand.Next(choices.Count);
            chordProg.Add(choices[idx]);
        }
        chordProg.Add("I");
        chordProg.Reverse();

        for (int i=0; i<chordProg.Count; i++)
        {
            Console.WriteLine(chordProg[i]);
        }

        return chordProg;
    }
}
