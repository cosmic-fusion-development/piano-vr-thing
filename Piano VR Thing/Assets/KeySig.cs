using System.Collections.Generic;

public class KeySig
{
    public string name;
    public string mode;
    public List<string> scale;

    private KeySig() {}

    public KeySig(string key)
    {
        name = key;

        switch (key)
        {
            case "C":
            scale = new List<string>(){"C", "D", "E", "F", "G", "A", "B"};
            break;

            case "a":
            scale = new List<string>(){"A", "B", "C", "D", "E", "F", "G"};
            break;

            case "C#":
            scale = new List<string>(){"C#", "D#", "E#", "F#", "G#", "A#", "B#"};
            break;

            case "a#":
            scale = new List<string>(){"A#", "B#", "C#", "D#", "E#", "F#", "G#"};
            break;

            case "Db":
            scale = new List<string>(){"Db", "Eb", "F", "Gb", "Ab", "Bb", "C"};
            break;

            case "bb":
            scale = new List<string>(){"Bb", "C", "Db", "Eb", "F", "Gb", "Ab"};
            break;

            case "D":
            scale = new List<string>(){"D", "E", "F#", "G", "A", "B", "C#"};
            break;

            case "b":
            scale = new List<string>(){"B", "C#", "D", "E", "F#", "G", "A"};
            break;

            case "Eb":
            scale = new List<string>(){"Eb", "F", "G", "Ab", "Bb", "C", "D"};
            break;

            case "c":
            scale = new List<string>(){"C", "D", "Eb", "F", "G", "Ab", "Bb"};
            break;

            case "E":
            scale = new List<string>(){"E", "F#", "G#", "A", "B", "C#", "D#"};
            break;

            case "c#":
            scale = new List<string>(){"C#", "D#", "E", "F#", "G#", "A", "B"};
            break;

            case "F":
            scale = new List<string>(){"F", "G", "A", "Bb", "C", "D", "E"};
            break;

            case "d":
            scale = new List<string>(){"D", "E", "F", "G", "A", "Bb", "C"};
            break;

            case "F#":
            scale = new List<string>(){"F#", "G#", "A#", "B", "C#", "D#", "E#"};
            break;

            case "d#":
            scale = new List<string>(){"D#", "E#", "F#", "G#", "A#", "B", "C#"};
            break;

            case "Gb":
            scale = new List<string>(){"Gb", "Ab", "Bb", "Cb", "Db", "Eb", "F"};
            break;

            case "eb":
            scale = new List<string>(){"Eb", "F", "Gb", "Ab", "Bb", "Cb", "Db"};
            break;

            case "G":
            scale = new List<string>(){"G", "A", "B", "C", "D", "E", "F#"};
            break;

            case "e":
            scale = new List<string>(){"E", "F#", "G", "A", "B", "C", "D"};
            break;

            case "Ab":
            scale = new List<string>(){"Ab", "Bb", "C", "Db", "Eb", "F", "G"};
            break;

            case "f":
            scale = new List<string>(){"F", "G", "Ab", "Bb", "C", "Db", "Eb"};
            break;

            case "A":
            scale = new List<string>(){"A", "B", "C#", "D", "E", "F#", "G#"};
            break;

            case "f#":
            scale = new List<string>(){"F#", "G#", "A", "B", "C#", "D", "E"};
            break;

            case "Bb":
            scale = new List<string>(){"Bb", "C", "D", "Eb", "F", "G", "A"};
            break;

            case "g":
            scale = new List<string>(){"G", "A", "Bb", "C", "D", "Eb", "F"};
            break;

            case "B":
            scale = new List<string>(){"B", "C#", "D#", "E", "F#", "G#", "A#"};
            break;

            case "g#":
            scale = new List<string>(){"G#", "A#", "B", "C#", "D#", "E", "F#"};
            break;

            case "Cb":
            scale = new List<string>(){"Cb", "Db", "Eb", "Fb", "Gb", "Ab", "Bb"};
            break;

            case "ab":
            scale = new List<string>(){"Ab", "Bb", "Cb", "Db", "Eb", "Fb", "Gb"};
            break;
        }
    }

    public int FindNote(string note)
    {
        int scaleNumber = -1;
        for (int i=0; i<7; i++)
        {
            if (scale[i] == note)
            {
                scaleNumber = i;
                break;
            }
        }

        return scaleNumber;
    }
}
