﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteFactory : MonoBehaviour
{
	MelodyFactory mFactory;
	public GameObject notePrefab;

	public void GenerateSheetMusic()
	{
		List<NoteObj> melody = mFactory.GenerateMelody();
		for (int i=0; i<melody.Count; i++)
		{
			NoteObj note = melody[i];

			Debug.Log(note.name + note.octNum.ToString());
			float x, y, z;
			z = 3.955f;

			if (note.name == "Rest")
			{
				y = LineOffsetToZ(6, note.mNo);
			}
			else
			{
				int startingLine = (note.octNum - 4) * 7;
				int offset = 0;
				switch (note.name[0])
				{
					case 'C':
					offset = 0;
					break;
					case 'D':
					offset = 1;
					break;
					case 'E':
					offset = 2;
					break;
					case 'F':
					offset = 3;
					break;
					case 'G':
					offset = 4;
					break;
					case 'A':
					offset = 5;
					break;
					case 'B':
					offset = 6;
					break;
				}
				y = LineOffsetToZ(startingLine + offset, note.mNo);
			}
			x = (GetHorizontalSpacing(note.mNo, (float)note.startBeat)*3f) - 1.4f;

			Vector3 pos = new Vector3(x, y, z);
			GameObject obj = Instantiate(notePrefab, pos, new Quaternion());
			obj.transform.localScale = new Vector3(0.15f, 0.1f, 0.025f);
			NoteHead notehead = obj.GetComponent<NoteHead>();
			notehead.SetNote(note);
		}
	}

	// Use this for initialization
	void Start ()
	{
		mFactory = new MelodyFactory(new KeySig("C"), new TimeSig(4, 4));
		GenerateSheetMusic();
	}

	// Update is called once per frame
	void Update ()
	{

	}

	float GetHorizontalSpacing(int mNo, float beatNum)
	{
		float start = 0f;
		if ((mNo % 2 == 0))
		{
			start = 0.5f;
		}

		return start+(beatNum-1f)*0.125f;
	}

	float LineOffsetToZ(int lineOffset, int mNo)
	{
		float start = 0f;
		switch (mNo)
		{
			case 1:
			case 2:
			start = 5.1875f;
			break;

			case 3:
			case 4:
			start = 3.6875f;
			break;

			case 5:
			case 6:
			start = 2.1825f;
			break;

			case 7:
			case 8:
			start = 0.6825f;
			break;
		}
		return start + (lineOffset*0.0625f);
	}
}
