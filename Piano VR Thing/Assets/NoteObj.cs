public class NoteObj
{
    public string name;
    public int octNum;
    public int mNo;
    public double startBeat;
    public double noteDur;

    private NoteObj(){}

    public NoteObj(string _name, int _octNum, int _mNo, double _startBeat, double _noteDur)
    {
        name = _name;
        octNum = _octNum;
        mNo = _mNo;
        startBeat = _startBeat;
        noteDur = _noteDur;
    }

    public override string ToString()
    {
        // TODO
        return name + octNum.ToString() + " m:" + mNo.ToString() + "." + startBeat.ToString() + " d:" + noteDur.ToString();
    }
}
