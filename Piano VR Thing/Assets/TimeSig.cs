public class TimeSig
{
    public int Top;
    public int Bottom;

    private TimeSig() {}

    public TimeSig(int top, int bottom)
    {
        Top = top;
        Bottom = bottom;
    }
}
