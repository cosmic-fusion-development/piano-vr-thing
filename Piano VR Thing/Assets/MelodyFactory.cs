using System.Collections.Generic;
using System;

public class MelodyFactory
{
    private ChordProgressionFactory _cpFactory;
    private RhythmFactory _rFactory;
    private KeySig _keySig;
    private TimeSig _timeSig;

    private MelodyFactory() { }

    public MelodyFactory(KeySig keySig, TimeSig timeSig)
    {
        _cpFactory = new ChordProgressionFactory();
        _timeSig = timeSig;
        _keySig = keySig;
        _rFactory = new RhythmFactory(timeSig, "easy", 1);
    }

    public List<NoteObj> GenerateMelody()
    {
        Random rand = new Random();
        List<string> aChordProg = _cpFactory.GenerateChordProgression(8);
        List<Chord> chordProg  = new List<Chord>();
        for (int i=0; i<aChordProg.Count; i++)
        {
            chordProg.Add(new Chord(_keySig, new AbstractChord(aChordProg[i])));
        }

        List<double> rhythm = _rFactory.GenerateRhythm();
        List<NoteObj> melody = new List<NoteObj>();

        int mNo = 0;
        double beat = 0;
        string prevNote = "";
        string curNote = "";
        int prevOctNum = 0;
        int curOctNum = 4;
        bool isUp = true;

        for (int i=0; i<rhythm.Count; i++)
        {
            if (beat >= _timeSig.Top)
            {
                mNo++;
                beat = 0;
            }
            if (rhythm[i]>0)
            {
                if (mNo == 0 && beat == 0)
                {
                    // TODO Starting note
                    curNote = _keySig.name;
                    curOctNum = 4;
                }
                else if (beat == 2 || beat == 0)
                {
                    curNote = FindNearestChordNote(chordProg[mNo], prevNote, true, ref isUp);
                    //
                }
                else if (i == rhythm.Count-1)
                {
                    curNote = _keySig.name;
                    int prevScale = _keySig.FindNote(prevNote);
                    if (prevScale < 4)
                    {
                        // down
                        if ((prevNote == "C" || prevNote == "C#" || prevNote == "Db" || prevNote == "D" || prevNote == "D#" || prevNote == "Eb" || prevNote == "E")
                            && (curNote == "Cb" || curNote == "B" || curNote == "Bb" || curNote == "A#" || curNote == "A" || curNote == "Ab" || curNote == "G#" || curNote == "G"))
                        {
                            curOctNum = prevOctNum -1;
                        }
                        else
                        {
                            curOctNum = prevOctNum;
                        }
                    }
                    else
                    {
                        // up
                        if ((curNote == "C" || curNote == "C#" || curNote == "Db" || curNote == "D" || curNote == "D#" || curNote == "Eb" || curNote == "E")
                            && (prevNote == "Cb" || prevNote == "B" || prevNote == "Bb" || prevNote == "A#" || prevNote == "A" || prevNote == "Ab" || prevNote == "G#" || prevNote == "G"))
                        {

                            curOctNum = prevOctNum + 1;
                        }
                        else
                        {
                            curOctNum = prevOctNum;
                        }
                    }
                }
                else
                {
                    int scaleNumber = _keySig.FindNote(prevNote);
                    int offset = 0;
                    double sample = rand.NextDouble();
                    if (sample<0.6)
                    {
                        offset = -4;
                        isUp = false;
                    }
                    else if (sample < 0.15)
                    {
                        offset = -3;
                        isUp = false;
                    }
                    else if (sample < 0.28)
                    {
                        offset = -2;
                        isUp = false;
                    }
                    else if (sample < 0.48)
                    {
                        offset = -1;
                        isUp = false;
                    }
                    else if (sample < 0.52)
                    {
                        offset = 0;
                        isUp = false;
                    }
                    else if (sample < 0.72)
                    {
                        offset = 1;
                        isUp = true;
                    }
                    else if (sample < 0.85)
                    {
                        offset = 2;
                        isUp = true;
                    }
                    else if (sample < 0.94)
                    {
                        offset = 3;
                        isUp = true;
                    }
                    else
                    {
                        offset = 4;
                        isUp = true;
                    }

                    curNote = _keySig.scale[Math.Abs((scaleNumber+offset)%7)];
                    // TODO calculate octNum
                    if (!isUp)
                    {
                        // down
                        if ((prevNote == "C" || prevNote == "C#" || prevNote == "Db" || prevNote == "D" || prevNote == "D#" || prevNote == "Eb" || prevNote == "E")
                            && (curNote == "Cb" || curNote == "B" || curNote == "Bb" || curNote == "A#" || curNote == "A" || curNote == "Ab" || curNote == "G#" || curNote == "G"))
                        {
                            curOctNum = prevOctNum -1;
                        }
                        else
                        {
                            curOctNum = prevOctNum;
                        }
                    }
                    else
                    {
                        // up
                        if ((curNote == "C" || curNote == "C#" || curNote == "Db" || curNote == "D" || curNote == "D#" || curNote == "Eb" || curNote == "E")
                            && (prevNote == "Cb" || prevNote == "B" || prevNote == "Bb" || prevNote == "A#" || prevNote == "A" || prevNote == "Ab" || prevNote == "G#" || prevNote == "G"))
                        {
                            curOctNum = prevOctNum + 1;
                        }
                        else
                        {
                            curOctNum = prevOctNum;
                        }
                    }
                }

                if (curOctNum > 4)
                {
                    curOctNum = 4;
                }
                else if (curOctNum<3)
                {
                    curOctNum = 3;
                }
                melody.Add(new NoteObj(curNote, curOctNum, mNo+1, beat+1, rhythm[i]));
                prevNote = curNote;
                prevOctNum = curOctNum;
            }
            else
            {
                melody.Add(new NoteObj("Rest", 0, mNo+1, beat+1, rhythm[i]));
            }
            beat += Math.Abs(rhythm[i]);
        }

        for (int i=0; i<melody.Count; i++)
        {
            Console.WriteLine(melody[i]);
        }

        return melody;
    }

    public string FindNearestChordNote(Chord chord, string note, bool changeNote, ref bool isUp)
    {
        Random rand = new Random();
        List<string> notes = chord.GetNotes();
        int offset = 0;
        if (_ContainedInList(notes, note) && (!changeNote))
        {
            return note;
        }

        int scaleNumber = _keySig.FindNote(note);
        for (offset = 1; offset<7; offset++)
        {
            string upNote = _keySig.scale[(scaleNumber+offset)%7];
            string downNote = _keySig.scale[Math.Abs((scaleNumber-offset)%7)];
            bool upNoteInScale = _ContainedInList(notes, upNote);
            bool downNoteInScale = _ContainedInList(notes, downNote);
            if (upNoteInScale && downNoteInScale)
            {
                int coin = rand.Next(2);
                if (coin==0)
                {
                    isUp = true;
                    return upNote;
                }
                else
                {
                    isUp = false;
                    return downNote;
                }
            }
            else if (upNoteInScale)
            {
                isUp = true;
                return upNote;
            }
            else if (downNoteInScale)
            {
                isUp = false;
                return downNote;
            }
        }

        return "";
    }

    private bool _ContainedInList(List<string> chord, string note)
    {
        bool flag = false;
        for (int i=0; i<3; i++)
        {
            if (chord[i] == note)
            {
                flag = true;
            }
        }
        return flag;
    }
}
