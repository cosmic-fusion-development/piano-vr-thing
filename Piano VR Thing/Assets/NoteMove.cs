﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteMove : MonoBehaviour {

	float startTime = 0;
	float AbsoluteTime = 0;
	public TimeHandle TimeObj = null;
	Transform pos = null;

	// Use this for initialization
	void Start () {
		TimeObj = GameObject.Find("GameObject").GetComponent<TimeHandle>();
		pos = this.GetComponent<Transform>();
		startTime =  pos.position[0];
	}

	// Update is called once per frame
	void Update () {
		AbsoluteTime = TimeObj.AbsoluteTime;
		pos.position = new Vector3(((AbsoluteTime)*-1f)+startTime, pos.position[1], pos.position[2]);
	}
}
