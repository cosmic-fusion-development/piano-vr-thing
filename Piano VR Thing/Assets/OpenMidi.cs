﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using libmidi.net;
using libmidi.net.Base;
using libmidi.net.Enums;
using libmidi.net.Events;


public class OpenMidi : MonoBehaviour {


	public MidiEventCollection events = new MidiEventCollection();
	public string eventstring = "";
	public string[] eventstringlist = new string[100000];
	private IList<MidiEvent> tevents;
	public int tracks = 0;

	// Use this for initialization
	void Start () {

		MidiFile file = new MidiFile("Guilty Night, Guilty Kiss! - 2x.mid");

		events = file.Events;

		eventstring = file.DeltaTicksPerQuarterNote.ToString();

		tevents = events.GetTrackEvents(0);

		int eventindex = 0;

		NoteEvent TempNote1 = null;
		NoteEvent TempNote2 = null;
		GameObject TempObject = null;

		tracks = file.Tracks;

		for (int i = 0; i < tracks; i++) {

			switch (i) {
				case 5:
				case 6:
				case 7:
				case 13:
					break;
				default:
					continue;
			}

			tevents = events.GetTrackEvents(i);

			//eventstringlist[eventindex] = "Track Number : " + i.ToString();
			//eventindex++;

			for (int j = 0; j < tevents.Count; j++) {




				if (MidiEvent.IsNoteOn(tevents[j])) {

					TempNote1 = (NoteEvent)tevents[j];

					TempObject = (GameObject)Instantiate(Resources.Load("NotePrefab")
					,new Vector3((tevents[j].AbsoluteTime*1f),(TempNote1.NoteNumber)*2f,(10f*(i)))
					,Quaternion.identity);

					switch (i) {
						case 5:
						case 6:
						case 7:
						if (TempNote1 != null && TempNote2 != null) {
							if (TempNote1.NoteNumber > 35 && TempNote1.NoteNumber < 85) {
								if (TempNote1.NoteNumber == TempNote2.NoteNumber) {
									var noteL = TempNote2.AbsoluteTime - TempNote1.AbsoluteTime;
									TempObject.transform.localScale += new Vector3(noteL*1f, 0, 0);
								}
							}

						}
						break;
						default:

						break;

					}

					switch (i) {
						case 5:
							TempObject.GetComponent<Renderer>().material.color = Color.blue;
							break;
						case 6:
							TempObject.GetComponent<Renderer>().material.color = Color.red;
							break;
						case 7:
							TempObject.GetComponent<Renderer>().material.color = Color.yellow;
							break;
						default:
						TempObject.GetComponent<Renderer>().material.color = Color.yellow;
						break;

					}



				}

				if (MidiEvent.IsNoteOff(tevents[j])) {

					TempNote2 = (NoteEvent)tevents[j];
					switch (i) {
						case 5:
						case 6:
						case 7:
						if (TempNote1.NoteNumber > 35 && TempNote1.NoteNumber < 85) {
							if (TempNote1.NoteNumber == TempNote2.NoteNumber) {
								var noteL = TempNote2.AbsoluteTime - TempNote1.AbsoluteTime;
								TempObject.transform.localScale += new Vector3(noteL*1f, 0, 0);
							}
						}
						break;
						default:
							TempObject.transform.localScale += new Vector3(2f, 2f, 2f);
						break;

					}

				}

				//eventstringlist[eventindex] = tevents[j].AbsoluteTime.ToString();
				//eventindex++;
			}
			//eventstringlist[eventindex] = "==========================================";
			//eventindex++;
		}


 		//System.IO.File.WriteAllLines("WriteLines.txt", eventstringlist);



	}

	// Update is called once per frame
	void Update () {

	}
}
