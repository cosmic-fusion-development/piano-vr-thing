﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteHead : MonoBehaviour {

	public string name;
	public int octNum;
	public int mNo;
	public double startBeat;
	public double noteDur;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public void SetNote(NoteObj note)
	{
		name = note.name;
		octNum = note.octNum;
		mNo = note.mNo;
		startBeat = note.startBeat;
		noteDur = note.noteDur;
	}
}
