using System;
using System.Collections.Generic;


public class RhythmFactory
{
    public TimeSig timeSig;
    public string difficulty;
    public double shortestNote;

    public RhythmFactory(TimeSig _timeSig, string _difficulty, double _shortestNote)
    {
        timeSig = _timeSig;
        difficulty = _difficulty;
        shortestNote = _shortestNote;
    }

    private RhythmFactory() {}

    public List<double> GenerateRhythm()
    {
        List<double> rhythm = new List<double>();
        Random rand = new Random();
        Dictionary<double, List<List<double> > > rhythms44 = new Dictionary<double, List<List<double> > >();
        // 1 beat patterns
        List<List<double> > oneBeat = new List<List<double> > ();
        oneBeat.Add(new List<double>(){1});
        oneBeat.Add(new List<double>(){.5, .5});
        oneBeat.Add(new List<double>(){-1});
        oneBeat.Add(new List<double>(){.5, -.5});
        rhythms44.Add(1, oneBeat);

        // 2 beat patterns
        List<List<double> > twoBeat = new List<List<double> > ();
        twoBeat.Add(new List<double>(){2});
        twoBeat.Add(new List<double>(){-2});
        twoBeat.Add(new List<double>(){0.5, 1, 0.5});
        twoBeat.Add(new List<double>(){1.5, 0.5});
        rhythms44.Add(2, twoBeat);

        // 3 beat patterns
        List<List<double> > threeBeat = new List<List<double> > ();
        threeBeat.Add(new List<double>(){3});
        threeBeat.Add(new List<double>(){-3});
        rhythms44.Add(3, threeBeat);

        // 4 beat patterns
        List<List<double> > fourBeat = new List<List<double> > ();
        fourBeat.Add(new List<double>(){4});
        fourBeat.Add(new List<double>(){-4});
        rhythms44.Add(4, fourBeat);

        bool firstNote = true;

        for (int mNo = 1; mNo <= 8; mNo++)
        {
            if (timeSig.Bottom == 4)
            {
                double beatsRemaining = timeSig.Top;
                while (beatsRemaining > 0)
                {
                    double sample = rand.NextDouble();
                    double beat = 1;
                    double probability = 2.0 / (beatsRemaining*(beatsRemaining+1));
                    for (int i=1; i<=beatsRemaining; i++)
                    {
                        if (sample < i*probability)
                        {
                            beat = i;
                            break;
                        }
                    }

                    beatsRemaining -= beat;
                    int idx = rand.Next(rhythms44[beat].Count);
                    List<double> pattern = rhythms44[beat][idx];
                    // Maybe flip the rests
                    sample = rand.NextDouble();
                    if (sample < 0.4 || firstNote || (mNo == 8 && beatsRemaining==0))
                    {
                        firstNote = false;
                        for (int i=0; i<pattern.Count; i++)
                        {
                            pattern[i] = Math.Abs(pattern[i]);
                        }
                    }
                    // Print the thing ah fuck
                    for (int i=0; i<pattern.Count; i++)
                    {
                        for (int j=0; j<Math.Abs(pattern[i])*2; j++)
                        {
                            if (pattern[i] < 0)
                            {
                                Console.Write("-");
                            }
                            else
                            {
                                Console.Write("x");
                            }
                        }
                        Console.Write(" ");
                        rhythm.Add(pattern[i]);
                    }
                }
            }
            else if (timeSig.Bottom == 8)
            {

            }

            Console.WriteLine("|");
        }
        return rhythm;
    }
}
