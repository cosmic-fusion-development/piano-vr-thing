﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using libmidi.net;
using libmidi.net.Base;
using libmidi.net.Enums;
using libmidi.net.Events;

public class TimeHandle : MonoBehaviour {

	public float AbsoluteTime = 0;
	public int BPM = 135;
	public float deltaTicksPerQuarterNote = 480;
	public float AbsoluteTimeSec = 0;


	// Use this for initialization
	void Start () {
		MidiFile file = new MidiFile("Guilty Night, Guilty Kiss! - 2x.mid");
		deltaTicksPerQuarterNote = file.DeltaTicksPerQuarterNote;

	}

	// Update is called once per frame
	void FixedUpdate () {



		AbsoluteTimeSec += Time.deltaTime;

		AbsoluteTime = ( AbsoluteTimeSec / 60  ) * ( deltaTicksPerQuarterNote * BPM );



	}
}
