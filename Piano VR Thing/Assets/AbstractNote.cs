using System;

public class AbstractNote
{
  public int _NoteNum;
  private AbstractNote() {}

  public AbstractNote(string scaleNumName)
  {
    switch(scaleNumName)
    {
      case "I":
      case "i":
      case "VII#":
      case "vii#":
      _NoteNum = 0;
      break;

      case "I#":
      case "i#":
      case "IIb":
      case "iib":
      _NoteNum = 1;
      break;

      case "II":
      case "ii":
      _NoteNum = 2;
      break;

      case "II#":
      case "ii#":
      case "IIIb":
      case "iiib":
      _NoteNum = 3;
      break;

      case "III":
      case "iii":
      case "IVb":
      case "ivb":
      _NoteNum = 4;
      break;

      case "IV":
      case "iv":
      case "III#":
      case "iii#":
      _NoteNum = 5;
      break;

      case "IV#":
      case "iv#":
      case "Vb":
      case "vb":
      _NoteNum = 6;
      break;

      case "V":
      case "v":
      _NoteNum = 7;
      break;

      case "V#":
      case "v#":
      case "VIb":
      case "vib":
      _NoteNum = 8;
      break;

      case "VI":
      case "vi":
      _NoteNum = 9;
      break;

      case "VI#":
      case "vi#":
      case "VIIb":
      case "viib":
      _NoteNum = 10;
      break;

      case "VII":
      case "vii":
      case "Ib":
      case "ib":
      _NoteNum = 11;
      break;
    }

    _NoteNum = -1;
  }

  public AbstractNote(int scaleNum)
  {
    if (scaleNum >= 0 && scaleNum < 12)
    {
      _NoteNum = scaleNum;
    }
    else
    {
      _NoteNum = -1;
    }
  }

  public static AbstractNote operator +(AbstractNote lhs, int rhs)
  {
    return new AbstractNote((lhs._NoteNum + rhs) % 12);
  }

  public static AbstractNote operator -(AbstractNote lhs, int rhs)
  {
    return new AbstractNote(Math.Abs((lhs._NoteNum - rhs) % 12));
  }

  public static bool operator ==(AbstractNote lhs, string rhs)
  {
    return (lhs.GetScaleNumString() == rhs);
  }

  public static bool operator !=(AbstractNote lhs, string rhs)
  {
    return (lhs.GetScaleNumString() != rhs);
  }

  public string GetScaleNumString()
  {
    switch(_NoteNum)
    {
      case 0:
      return "I";

      case 2:
      return "II";

      case 4:
      return "III";

      case 5:
      return "IV";

      case 7:
      return "V";

      case 9:
      return "VI";

      case 11:
      return "VII";
    }

    return "";
  }
}
