using System;
using System.Collections.Generic;


public class Chord
{
    public KeySig keySig;
    public AbstractChord abstractChord;

    private Chord() {}

    public Chord(KeySig ks, AbstractChord ac)
    {
        keySig = ks;
        abstractChord = ac;
    }

    public List<string> GetNotes()
    {
        List<string> notes = new List<string>();

        AbstractNote aNote = abstractChord._root;

        int rootIdx = -1;
        switch(aNote._NoteNum)
        {
            case 0:
            rootIdx = 0;
            break;

            case 2:
            rootIdx = 1;
            break;

            case 4:
            rootIdx = 2;
            break;

            case 5:
            rootIdx = 3;
            break;

            case 7:
            rootIdx = 4;
            break;

            case 9:
            rootIdx = 5;
            break;

            case 11:
            rootIdx = 6;
            break;
        }

        notes.Add(keySig.scale[rootIdx]);
        notes.Add(keySig.scale[(rootIdx+2)%7]);
        notes.Add(keySig.scale[(rootIdx+4)%7]);

        return notes;
    }
}
